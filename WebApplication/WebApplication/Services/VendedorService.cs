using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendedorService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public VendedorService(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<Vendedor> Listar()
        {
            return _context.Vendedores.Where(x => x.Ativo == true).ToList();
        }

        public Vendedor RetornaPorId(int id)
        {
            return _context.Vendedores.Where(x => x.Id == id).FirstOrDefault();
        }

        public Vendedor RetornaPorNome(string nome)
        {
            return _context.Vendedores.Where(x => x.Nome.Contains(nome)).FirstOrDefault();
        }

        public Vendedor RetornaPorCpf(string cpf)
        {
            return _context.Vendedores.Where(x => x.Cpf == cpf).FirstOrDefault();
        }

        public Vendedor CriarVendedor(VendedorDTO vendedorDTO)
        {
            var novoVendedor = _mapper.Map<Vendedor>(vendedorDTO);
            novoVendedor.Ativo = true;
            _context.Vendedores.Add(novoVendedor);
            _context.SaveChanges();
            return novoVendedor;
        }

        public Vendedor EditarVendedor(VendedorDTO vendedorDTO)
        {
            var editVendedor = _mapper.Map<Vendedor>(vendedorDTO);
            _context.Vendedores.Update(editVendedor);
            _context.SaveChanges();
            return editVendedor;
        }
        
        public Vendedor ExcluirVendedor(int id)
        {
            var delVendedor = _context.Vendedores.Where(x => x.Id == id).FirstOrDefault();
            delVendedor.Ativo = false;
            _context.SaveChanges();
            return delVendedor;
        }
    }
}