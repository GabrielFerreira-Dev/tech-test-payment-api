using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.Context;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class ItemService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ItemService(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<Item> Listar()
        {
            return _context.Itens.Where(x => x.Ativo == true).ToList();
        }

        public Item RetornaPorId(int id)
        {
            return _context.Itens.Where(x => x.Id == id).FirstOrDefault();
        }

        public Item RetornaPorNome(string nome)
        {
            return _context.Itens.Where(x => x.Nome.Contains(nome)).FirstOrDefault();
        }

        public Item CriarItem(ItemDTO itemDTO)
        {
            var novoItem = _mapper.Map<Item>(itemDTO);
            novoItem.Ativo = true;
            _context.Itens.Add(novoItem);
            _context.SaveChanges();
            return novoItem;
        }

        public Item EditarItem(ItemDTO itemDTO)
        {
            var editItem = _mapper.Map<Item>(itemDTO);
            _context.Itens.Update(editItem);
            _context.SaveChanges();
            return editItem;
        }
        
        public Item ExcluirItem(int id)
        {
            var delItem = _context.Itens.Where(x => x.Id == id).FirstOrDefault();
            delItem.Ativo = false;
            _context.SaveChanges();
            return delItem;
        }
    }
}