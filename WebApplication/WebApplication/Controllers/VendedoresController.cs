﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class VendedoresController : ControllerBase
    {
        private readonly VendedorService _vendedorService;

        public VendedoresController(VendedorService vendedorService)
        {
            _vendedorService = vendedorService;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var listaBanco = _vendedorService.Listar();
            return Ok(listaBanco);
        }

        [HttpGet("BuscarPorId")]
        public IActionResult RetornarPorId(int id)
        {
            var vendedorId = _vendedorService.RetornaPorId(id);
            return Ok(vendedorId);
        }

        [HttpGet("BuscarPorNome")]
        public IActionResult RetornarPorNome(string nome)
        {
            var vendedorNome = _vendedorService.RetornaPorNome(nome);
            return Ok(vendedorNome);
        }

        [HttpGet("BuscarPorCpf")]
        public IActionResult RetornarPorCpf(string cpf)
        {
            var vendedorCpf = _vendedorService.RetornaPorCpf(cpf);
            return Ok(vendedorCpf);
        }

        [HttpPost]
        public IActionResult CriarVendedor(VendedorDTO vendedorDTO)
        {
            var novoVendedor = _vendedorService.CriarVendedor(vendedorDTO);
            return Ok(novoVendedor);
        }

        [HttpPut]
        public IActionResult EditarVendedor(VendedorDTO vendedorDTO)
        {
            var editVendedor = _vendedorService.EditarVendedor(vendedorDTO);
            return Ok(editVendedor);
        }

        [HttpDelete]
        public IActionResult ExcluirItem(int id)
        {
            var delVendedor = _vendedorService.ExcluirVendedor(id);
            return Ok("Item excluído");
        }
    }
}

