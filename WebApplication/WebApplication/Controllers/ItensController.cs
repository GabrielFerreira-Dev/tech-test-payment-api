using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    public class ItensController : ControllerBase
    {
        private readonly ItemService _itemService;

        public ItensController(ItemService itemService)
        {
            _itemService = itemService;
        }

        [HttpGet]
        public IActionResult Listar()
        {
            var listaBanco = _itemService.Listar();
            return Ok(listaBanco);
        }

        [HttpGet("BuscarPorId")]
        public IActionResult RetornarPorId(int id)
        {
            var itemId = _itemService.RetornaPorId(id);
            return Ok(itemId);
        }

        [HttpGet("BuscarPorNome")]
        public IActionResult RetornarPorNome(string nome)
        {
            var itemNome = _itemService.RetornaPorNome(nome);
            return Ok(itemNome);
        }

        [HttpPost]
        public IActionResult CriarItem (ItemDTO itemDTO)
        {
            var novoItem = _itemService.CriarItem(itemDTO);
            return Ok(novoItem);
        }

        [HttpPut]
        public IActionResult EditarItem (ItemDTO itemDTO)
        {
            var editItem = _itemService.EditarItem(itemDTO);
            return Ok(editItem);
        }

        [HttpDelete]
        public IActionResult ExcluirItem (int id)
        {
            var delItem = _itemService.ExcluirItem(id);
            return Ok("Item excluído");
        }
    }
}